json.array!(@postes) do |poste|
  json.extract! poste, :id, :title, :body
  json.url poste_url(poste, format: :json)
end
