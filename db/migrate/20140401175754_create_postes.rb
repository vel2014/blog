class CreatePostes < ActiveRecord::Migration
  def change
    create_table :postes do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
